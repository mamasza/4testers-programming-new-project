# Listy zawierające dane do losowania
import random
from datetime import date

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def generate_random_fname(list_of_fnames):
    random_fname = random.choice(list_of_fnames)
    return random_fname


def generate_random_surname(list_of_surnames):
    random_surname = random.choice(list_of_surnames)
    return random_surname


def generate_random_country(list_of_countries):
    random_country = random.choice(list_of_countries)
    return random_country


def generate_random_email(fname, surname):
    return f'{fname.lower()}.{surname.lower()}@example.dot'


def generatate_random_age():
    return random.randint(5, 45)


def is_an_adult(age):
    return True if age >= 18 else False


def calculate_year_of_the_birth(age):
    return date.today().year - age

def generate_person_data(is_female):
    firstname = random.choice(female_fnames) if is_female else random.choice(male_fnames)
    surname = generate_random_surname(surnames)
    email = generate_random_email(firstname, surname)
    age = generatate_random_age()
    return {
        'firstname': firstname,
        'surname': surname,
        'country': generate_random_country(countries),
        'email': email,
        'age': age,
        'adult': is_an_adult(age),
        'birth_date': calculate_year_of_the_birth(age)
    }


def generate_list_of_people(number_of_people):
    list_of_people = []
    for i in range(number_of_people):
        female = bool(i % 2)
        person_dictionary = generate_person_data(female)
        list_of_people.append(person_dictionary)
    return list_of_people


def generate_and_print_welcom_message_for_all_people(list_of_people):
    for person in list_of_people:
        print(
            f'Hi! I am {person["firstname"]} {person["surname"]}.I come from {person["country"]} and I was born in {person["birth_date"]}')

if __name__ == '__main__':
    people = generate_list_of_people(10)
    generate_and_print_welcom_message_for_all_people(people)
